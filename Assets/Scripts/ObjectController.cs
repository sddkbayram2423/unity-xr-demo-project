
using UnityEngine;
using UnityEngine.InputSystem;

namespace DemoProject.Scripts
{

    [RequireComponent(typeof(Rigidbody))]
    public class ObjectController : MonoBehaviour
    {
        // Start is called before the first frame update

        [SerializeField] private InputActionReference jumpActionReference;
        [SerializeField] private float jumpForce = 1.0f;
        [SerializeField] private float distanToGround = 0.20f;

        private bool _isGrounded => Physics.Raycast(transform.position, Vector3.down, distanToGround + 0.1f);
        

        private Rigidbody _body;

        void Start()
        {
            _body = GetComponent<Rigidbody>();
            jumpActionReference.action.performed += OnJump;
        }


        void OnJump(InputAction.CallbackContext obj)
        {

            if (_isGrounded)
            {
                Vector3 force = Vector3.up * jumpForce;
                _body.AddForce(force, ForceMode.Impulse);

            }

        }
    }
}